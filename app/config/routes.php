<?php

return array(

    'login' => 'users/login',
    'logout' => 'users/logout',

    'portfolio/p([0-9]+)' => 'portfolio/index/$1',
    'portfolio/([0-9]+)' => 'portfolio/viewItem/$1',
    'portfolio/additem' => 'portfolio/addItem',
    'portfolio' => 'portfolio/index',

    'blog' => 'blog/index',

    'contact' => 'contact/message'

);