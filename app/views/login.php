<!DOCTYPE html>
<html lang="ru-RU">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  <link rel="stylesheet" href="../../app/assets/stylesheets/application.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
</head>
<body>

<?php require_once 'templates/main_header.php'; ?>

<?php if ( !isset( $_SESSION['user'] ) ): ?>
  <div class="wrapper wrapper_center">
    <?php if ( isset($errors) && is_array($errors) ): ?>
      <?php echo $errors[0]; ?>
    <?php endif; ?>
    <div class="errors">
    </div>
    <form action="" method="post">
      <label for="email">Email:</label><br>
      <input type="text" name="email"><br>
      <br>
      <label for="password">Пароль:</label><br>
      <input type="password" name="password" autocomplete="off"><br>
      <br>
      <input type="submit" name="login-submit" value="Вход">
    </form>
  </div>
<?php else: ?>
  <?php header('Location: /'); ?>
<?php endif; ?>

<?php require_once 'templates/main_footer.php'; ?>