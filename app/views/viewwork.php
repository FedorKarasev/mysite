<!DOCTYPE html>
<html lang="ru-RU">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="../../app/assets/stylesheets/application.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
</head>
<body>

<?php require_once 'templates/main_header.php'; ?>

<div class="wrapper wrapper_center">

    <section class="portfolio portfolio-page clearfix">
        <h2><?php echo $portfolio_item[0]['title']; ?></h2>
        <hr>
        <p>www.google.com</p>
        <div class="one_item_view">
          <img src="<?php echo $portfolio_item[0]['image']; ?>" alt="">
          <p><?php echo $portfolio_item[0]['description'] ?></p>
        </div>
    </section>

</div>

<?php require_once 'templates/main_footer.php'; ?>
