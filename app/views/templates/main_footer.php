<footer id="main_footer" class="clearfix">
    <div class="copyright">
        <p>&copy; Fedor Karasev 2016</p>
    </div>
    <div class="socials">
        <a href=""><i class="fa fa-vk" aria-hidden="true"></i></a>
        <a href=""><i class="fa fa-instagram" aria-hidden="true"></i></a>
        <a href=""><i class="fa fa-github" aria-hidden="true"></i></a>
    </div>
</footer>

<?php require_once 'javascripts.php'; ?>


</body>
</html>
