<header id="main_header" class="clearfix">
    <a href="/" class="logo">Fedor Karasev</a>
    <nav>
        <a href="/portfolio">Портфолио</a>
        <a href="/contact">Контакты</a>
        <a href="/blog">Блог</a>
        <?php if ( isset( $_SESSION['user'] ) ): ?>
            <?php echo '<a href="/logout/">Выход</a>' ?>
        <?php endif; ?>
    </nav>
</header>