<!DOCTYPE html>
<html lang="ru-RU">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  <link rel="stylesheet" href="../../app/assets/stylesheets/application.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
</head>
<body>

<?php require_once 'templates/main_header.php'; ?>

  <div class="wrapper">

    <section class="portfolio portfolio-page clearfix">
      <h2>Портфолио</h2>
      <hr>
      <?php if ( UserModel::getUserID() ): ?>
        <?php echo '<a href="/portfolio/additem">(Добавить работу)</a>'; ?>
      <?php endif; ?>
      <div class="portfolio-wrap clearfix">

        <?php foreach ($portfolio_items as $item): ?>
          <div class="portfolio-item">
            <h3><a href="<?php echo '/portfolio/'.$item['id']; ?>"><?php echo $item['title']; ?></a></h3>
            <img src="<?php echo $item['image']; ?>" alt="">
          </div>
        <?php endforeach; ?>

      </div>

      <div class="portfolio_pagination_wrap">
        <div class="pagination">
          <span class="previous_page disabled">Предыдущая</span>
          <em class="current">1</em>
          <a rel="next" href="">2</a>
          <a href="" class="next_page">Следующая</a>
        </div>
      </div>
      
    </section>

  </div>

<?php require_once 'templates/main_footer.php'; ?>