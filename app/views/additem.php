<!DOCTYPE html>
<html lang="ru-RU">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="../../app/assets/stylesheets/application.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
</head>
<body>

<?php require_once 'templates/main_header.php'; ?>

<?php if ( $_SESSION['user'] ): ?>
    <div class="wrapper wrapper_center">
        <?php if ( isset($errors) && is_array($errors) ): ?>
            <?php echo $errors[0]; ?>
        <?php endif; ?>
        <div class="errors">
        </div>
        <form action="" method="post" enctype="multipart/form-data">
            <label for="email">Название:</label><br>
            <input type="text" name="title"><br>
            <br>
            <label for="password">Изображение:</label><br>
            <input type="file" name="image"><br>
            <br>
            <label for="password">Ссылка:</label><br>
            <input type="text" name="link"><br>
            <br>
            <label for="password">Описание:</label><br>
            <textarea name="description"></textarea>
            <br>
            <input type="submit" name="additem-submit" value="Отправить">
        </form>
    </div>
<?php else: ?>
    <?php header('Location: /'); ?>
<?php endif; ?>

<?php require_once 'templates/main_footer.php'; ?>