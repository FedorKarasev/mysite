<?php

$name = $_POST['name'];

if ($name === '') {
  $data['status'] = 'Error';
  $data['text'] = 'Заполните имя';
} else {
  $data['status'] = 'OK';
}

header("Content-Type: application/json");

echo json_encode($data);
