<!DOCTYPE html>
<html lang="ru-RU">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  <link rel="stylesheet" href="../../app/assets/stylesheets/application.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
</head>
<body>

<?php require_once 'templates/main_header.php'; ?>

  <div class="wrapper wrapper_center">
    <div class="errors"></div>
    <form id="send_message_form" action="" method="POST">
      <label for="name">Имя:<sup>*</sup></label><br>
      <input type="text" name="name"><br>
      <br>
      <label for="email">Email:<sup>*</sup></label><br>
      <input type="text" name="email"><br>
      <br>
      <label for="phone">Телефон:</label><br>
      <input type="text" name="phone"><br>
      <br>
      <label for="message_text">Сообщение<sup>*</sup></label><br>
      <textarea name="message_text"></textarea><br>
      <br>
      <input type="submit" name="submit_form" value="Отправить сообщение">
    </form>
  </div>

<?php require_once 'templates/main_footer.php'; ?>