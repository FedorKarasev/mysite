<!DOCTYPE html>
<html lang="ru-RU">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  <link rel="stylesheet" href="../../app/assets/stylesheets/application.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
</head>
<body>

<?php require_once 'templates/main_header.php'; ?>

<div class="wrapper">

  <section class="about_me clearfix">
    <div class="photo">
      <img src="../../app/assets/images/ava.jpg" alt="">
    </div>
    <div class="information">
      <h2>Обо мне</h2>
      <div class="row clearfix">
        <div class="item">
          <div class="left">
            <i class="fa fa-user" aria-hidden="true"></i>
          </div>
          <div class="right">
            <h3>Имя:</h3>
            <p>Федор Карасев</p>
          </div>
        </div>
        <div class="item">
          <div class="left">
            <i class="fa fa-envelope" aria-hidden="true"></i>
          </div>
          <div class="right">
            <h3>Email:</h3>
            <a href="mailto:xenos12@gmail.com">xenos12@gmail.com</a>
          </div>
        </div>
        <div class="item">
          <div class="left">
            <i class="fa fa-mobile" aria-hidden="true"></i>
          </div>
          <div class="right">
            <h3>Телефон:</h3>
            <a href="tel:+79670483565">(967)-048-35-65</a>
          </div>
        </div>
      </div>
      <div class="row">
        <p>Привет. Меня зовут Федор Карасев.
          Я занимаюсь front-end и back-end web-разработкой
          около года и постоянно в этом совершенствуюсь.
          Моя цель - создание замечательных проектов как
          с инженерной, так и визуальной точки зрения.</p>
      </div>
    </div>
  </section>

  <section class="services clearfix">
    <h2>Услуги</h2>
    <div class="service-item">
      <i class="fa fa-desktop" aria-hidden="true"></i>
      <h3>Front-end разработка</h3>
      <p>Front end development is the second most important
        aspect of a site, right behind back end. No one wants
        to do business with someone with a hideous website.
        A beautiful design is my personal goal for every project.
      </p>
    </div>
    <div class="service-item">
      <i class="fa fa-code" aria-hidden="true"></i>
      <h3>Back-end разработка</h3>
      <p>Front end development is the second most important
        aspect of a site, right behind back end. No one wants
        to do business with someone with a hideous website.
        A beautiful design is my personal goal for every project.
      </p>
    </div>
    <div class="service-item">
      <i class="fa fa-wordpress" aria-hidden="true"></i>
      <h3>WordPress</h3>
      <p>Front end development is the second most important
        aspect of a site, right behind back end. No one wants
        to do business with someone with a hideous website.
        A beautiful design is my personal goal for every project.
      </p>
    </div>
    <div class="service-item">
      <i class="fa fa-suitcase" aria-hidden="true"></i>
      <h3>Техническая поддержка</h3>
      <p>Front end development is the second most important
        aspect of a site, right behind back end. No one wants
        to do business with someone with a hideous website.
        A beautiful design is my personal goal for every project.
      </p>
    </div>
  </section>

  <section class="portfolio clearfix">
    <h2>Портфолио</h2>
    <div class="portfolio-wrap clearfix">

      <?php foreach ($portfolio_items as $item): ?>
      <div class="portfolio-item">
        <h3><a href="<?php echo '/portfolio/'.$item['id']; ?>"><?php echo $item['title']; ?></a></h3>
        <img src="<?php echo $item['image']; ?>" alt="">
      </div>
      <?php endforeach; ?>

    </div>
    <div class="portfolio_btn_wrap">
      <a href="/portfolio/" class="button">Смотреть все</a>
    </div>
  </section>

</div>

<?php require_once 'templates/main_footer.php'; ?>