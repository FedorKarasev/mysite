<?php

class PortfolioModel
{
    public static function getAllItems($page = 1)
    {

        $page = intval($page);
        $offset = ($page - 1) * 16;

        $db = Database::get_connection();

        $sql = "SELECT * FROM myworks ORDER BY id DESC LIMIT 16 OFFSET :offset";

        $result= $db->prepare($sql);
        $result->bindParam(':offset', $offset, PDO::PARAM_INT);
        $result->execute();
        $result->setFetchMode(PDO::FETCH_ASSOC);

        $i = 0;
        while( $row = $result->fetch() ) {
            $portfolio[$i]['id'] = $row['id'];
            $portfolio[$i]['title'] = $row['title'];
            $portfolio[$i]['image'] = $row['image'];
            $portfolio[$i]['link'] = $row['link'];
            $portfolio[$i]['description'] = $row['description'];
            $i++;
        }

        return $portfolio;
    }

    public static function getLast4Items()
    {

        $db = Database::get_connection();

        $sql = "SELECT * FROM myworks ORDER BY id DESC LIMIT 4";

        $result= $db->prepare($sql);
        $result->execute();
        $result->setFetchMode(PDO::FETCH_ASSOC);

        $i = 0;
        while( $row = $result->fetch() ) {
            $portfolio[$i]['id'] = $row['id'];
            $portfolio[$i]['title'] = $row['title'];
            $portfolio[$i]['image'] = $row['image'];
            $portfolio[$i]['link'] = $row['link'];
            $portfolio[$i]['description'] = $row['description'];
            $i++;
        }

        return $portfolio;
    }

    public static function getItemById($id)
    {
        $db = Database::get_connection();

        $sql = "SELECT * FROM myworks WHERE id= :id";

        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->execute();
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result = $result->fetchAll();

        return $result;
    }

    public static function getTotalItems()
    {
        $db = Database::get_connection();

        $result = $db->query('SELECT count(id) AS count FROM myworks');
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $row = $result->fetch();

        return $row['count'];
    }

    public static function addItem($parameters)
    {
        $db = Database::get_connection();

        $sql = "INSERT INTO myworks (title, image, link, description)
                VALUES (:title, :image, :link, :description)";

        $result = $db->prepare($sql);
        $result->bindParam(':title', $parameters['title'], PDO::PARAM_STR);
        $result->bindParam(':image', $parameters['image'], PDO::PARAM_STR);
        $result->bindParam(':link', $parameters['link'], PDO::PARAM_STR);
        $result->bindParam(':description', $parameters['description'], PDO::PARAM_STR);
        $result->execute();

        return $result;
    }

    public static function editItem($id, $parameters)
    {
        $db = Database::get_connection();

        $sql = "UPDATE myworks SET
                title = :title, 
                image = :image
                link = :link, 
                description = :description 
                WHERE id = :id";

        $result = $db->prepare($sql);
        $result->bindParam(':title', $parameters['title'], PDO::PARAM_STR);
        $result->bindParam('', $parameters['link'], PDO::PARAM_STR);
        $result->bindParam('', $parameters['description'], PDO::PARAM_STR);
    }
}