<?php

class UserModel
{
    public static function checkEmail($email)
    {

    }

    public static function checkPassword($password)
    {

    }

    public static function checkUserData($email, $password)
    {
        $db = Database::get_connection();

        $sql = 'SELECT * FROM users WHERE email = :email AND password = :password';

        $result = $db->prepare($sql);
        $result->bindParam(':email', $email, PDO::PARAM_STR);
        $result->bindParam(':password', $password, PDO::PARAM_STR);
        $result->execute();

        $user =  $result->fetch();

        if ( $user ) {
            return $user['id'];
        } else {
            return false;
        }
    }

    public static function Auth($userID)
    {
        $_SESSION['user'] = $userID;
    }

    public static function getUserID()
    {
        if ( isset( $_SESSION['user'] ) ) {
            return $_SESSION['user'];
        }
    }

    public static function logout()
    {
        unset( $_SESSION['user'] );
        header('Location: /');
    }
}