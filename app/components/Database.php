<?php

class Database
{
    public static function get_connection()
    {

        $db_params_path = ROOT.'/app/config/db_params.php';
        $db_params = require $db_params_path;

        $dsn = "mysql:host={$db_params['host']};dbname={$db_params['dbname']}";
        $db = new PDO($dsn, $db_params['user'], $db_params['password']);
        $db->exec("set names utf8");

        return $db;
    }
}