<?php

class Router
{
    private $routes;

    private static function get_routes()
    {
        $routes_path = ROOT.'/app/config/routes.php';
        return $routes = require_once $routes_path;
    }

    private static function get_uri()
    {

        $uri = '';

        if ( !empty($_SERVER['REQUEST_URI']) ) {
            $uri = strtolower( trim($_SERVER['REQUEST_URI'], '/') );
        }

        return $uri;
    }

    private static function error_page_404() {
        header('');
    }

    public static function run()
    {

        $routes = self::get_routes();
        $uri = self::get_uri();

        $controller_name = 'WelcomeController';
        $action_name = 'actionIndex';
        $controller_file = ROOT.'/app/controllers/'.$controller_name.'.php';

        if ( $uri !== '' ) {

            foreach ($routes as $uri_pattern => $path) {

                if ( preg_match("~$uri_pattern~", $uri) ) {

                    $internal_route = preg_replace( "~$uri_pattern~", $path, $uri );

                    $segments = explode('/', $internal_route);

                    $controller_name = ucfirst( array_shift($segments) ).'Controller';
                    $action_name = 'action'.ucfirst( array_shift($segments) );

                    $controller_file = ROOT.'/app/controllers/'.$controller_name.'.php';

                    if ( file_exists($controller_file) ) {

                        require_once $controller_file;
                        call_user_func_array( array( $controller_name, $action_name ), $segments );
                        break;

                    }

                }

            }

        } else {

            require_once $controller_file;
            $controller_name::$action_name();

        }


    }
}