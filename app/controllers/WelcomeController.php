<?php

require_once ROOT.'/app/models/PortfolioModel.php';

class WelcomeController
{
    public static function actionIndex()
    {
        $portfolio_items = PortfolioModel::getLast4Items();

        require_once ROOT.'/app/views/index.php';
    }
}