<?php

class UsersController
{
    public static function actionLogin()
    {
        $email = null;
        $password = null;

        if ( isset( $_POST['login-submit'] ) ) {

            $email = $_POST['email'];
            $password = $_POST['password'];

            $errors = null;

            $userID = UserModel::checkUserData($email, $password);

            if ( $userID ) {
                UserModel::Auth($userID);
                header('Location: /');
            } else {
                $errors[] = 'Неправильные данные для входа на сайт';
            }

        }

        require_once ROOT.'/app/views/login.php';
    }

    public static function actionLogout()
    {
        UserModel::logout();
    }
}