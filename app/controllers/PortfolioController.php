<?php

require_once ROOT.'/app/models/PortfolioModel.php';

class PortfolioController
{
    public static function actionIndex($page = 1)
    {

        $portfolio_items = PortfolioModel::getAllItems($page);

        require_once ROOT.'/app/views/myworks.php';
    }

    public static function actionViewItem($id)
    {
        $portfolio_item = PortfolioModel::getItemById($id);

        require_once ROOT.'/app/views/viewwork.php';
    }

    public static function actionAddItem()
    {
        $parameters = null;
        $id = PortfolioModel::getTotalItems() + 1;

        if ( isset( $_POST['additem-submit'] ) ) {
            $parameters['title'] = $_POST['title'];
            $image = $_FILES['image'];
            print_r($image);
            if ( is_uploaded_file($image['tmp_name']) ) {
                echo 'OK<br>';
                move_uploaded_file($image['tmp_name'], ROOT.'/resources/upload/portfolio/'.$id.'.jpg');
            }
            $parameters['image'] = '/resources/upload/portfolio/'.$id.'.jpg';
            echo $parameters['image'];
            $parameters['link'] = $_POST['link'];
            $parameters['description'] = $_POST['description'];

            $result = PortfolioModel::addItem($parameters);

            if ( $result ) {
                echo 'Add OK<br>';
            } else {
                echo 'ERROR<br>';
            }
            header("Location: /portfolio/");
        }

        require_once ROOT.'/app/views/additem.php';
    }

}