<?php

ini_set('display_errors', 1);
error_reporting(E_ALL);

define('ROOT', dirname(__FILE__));
require_once ROOT.'/app/components/Autoload.php';

session_start();

Router::run();